package com.example.studentass.models

class TestQuestion (
    val id: Long,
    val question: String,
    val answerChoice: List<TestAnswer>
)