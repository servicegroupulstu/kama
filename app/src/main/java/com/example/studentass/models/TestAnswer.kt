package com.example.studentass.models

class TestAnswer (
    val id: Long,
    val answer: String
)