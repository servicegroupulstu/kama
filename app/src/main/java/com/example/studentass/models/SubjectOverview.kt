package com.example.studentass.models

class SubjectOverview (
    val subjectName: String,
    val teacherName: String,
    val progress: Int,
    val maxProgress: Int
)